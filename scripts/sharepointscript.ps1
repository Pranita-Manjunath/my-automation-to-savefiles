Connect-PnPOnline -Url https://medtronicapac.sharepoint.com/sites/DevOps_FileTransfer -ClientId 19a9f48f-364a-4d58-a0a9-23e2278458c4 -ClientSecret golLzqUQPnhc0/xqqjfZM9QoMGsxDyApi9Jm4Dih+mg=;

# Specify the path to the folder you want to upload
$FolderPath = "/Users/zmo-mac-pranitam-02/Documents/UT_Coverage_Reports"
$DocumentLibraryName = "Documents"

# Get the document library
$Library = Get-PnPList -Identity $DocumentLibraryName 

$Folder = Add-PnPFolder -Name (Split-Path $FolderPath -Leaf) -Folder $Library.RootFolder
Add-PnPFile -Path $FolderPath\* -Folder $Folder

$SubFolders = Get-ChildItem -Path $FolderPath -Directory
if ($SubFolders) {
    foreach ($SubFolder in $SubFolders) {
        $SubFolderPath = $SubFolder.FullName
        $SubFolderName = $SubFolder.Name
        $SubFolder = Add-PnPFolder -Name $SubFolderName -Folder $Folder
        Add-PnPFile -Path $SubFolderPath\* -Folder $SubFolder
    }
}

